<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        \Redis::publish('name', 'david');
        return view('welcome');
    }
}
