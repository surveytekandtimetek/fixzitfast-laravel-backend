var fs 		= require('fs');
var express = require('express');
var app 	= express();
debug 		= true;

var http 	= require('http').Server(app);
var url 	= '51.141.127.136';

var io 		= require('socket.io')(http);
var Redis 	= require('ioredis');
var redis 	= new Redis();
var PORT 	= 8080;


redis.subscribe('fixzitfast_database_name', function(data) {});

redis.on('message', function(channel, message) {
    if (debug === true) {
    	console.log('Message Recieved on channel ' + channel + ': ' + message);
    }
    io.emit(channel, message);
});
io.on('connect', (socket) => {
	console.log('SOCKET ID: ' + socket.id);
	console.log('SOCKET COUNT: ' + io.engine.clientsCount);
	io.sockets.emit('online', socket.id);
});
http.listen(PORT, function(){
	if (debug === true) {
		console.log(url + ' listening on Port ' + PORT);
	}
});
